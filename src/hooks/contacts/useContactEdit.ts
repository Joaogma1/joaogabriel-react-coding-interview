import { IPerson } from '@lib/models/person';
import {useCallback, useEffect, useState} from 'react';
import { contactsClient } from '@lib/clients/contacts';
export function useContactEdit(id: string) {
  console.log(`Getting contact by id ${id}`);

  const [person, setPerson] = useState<IPerson>(null);

  useEffect(() => {
    contactsClient.getContactById(id)
        .then(value => setPerson(value))
    return () => setPerson(null)
  },[])

  const saveChanges = () => contactsClient.updateContact(id, person)

  return {
    contact: person as IPerson,
    update: useCallback((updated: IPerson) => {
      console.log(updated);
      setPerson(updated)
    }, []),
    saveChanges
  };
}
