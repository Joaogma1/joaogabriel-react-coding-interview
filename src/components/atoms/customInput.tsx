import { TextField, styled } from '@mui/material';

export const CustomInput = styled(TextField)(({ theme }) => ({
    width: "400px",
    margin: '4px'
}));
