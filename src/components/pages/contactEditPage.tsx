import react, {useEffect, useMemo, useState} from 'react'
import {useContactEdit} from "@hooks/contacts/useContactEdit";
import {useNavigate, useParams} from "react-router-dom";
import {CentralizedForm} from "@components/organisms";

import {CustomInput} from "@components/atoms/customInput";

const ContactEditPage : react.FC = () => {
    const {id} = useParams()
    const navigate = useNavigate()
    const {contact,update,saveChanges} = useContactEdit(id)
    const [errorMessage, setErrorMessage] = useState<string>('')
    const isEmailField = (value: string) : boolean => value === 'email';
    const isId = (value: string) : boolean => value === 'id'
    const renderFieldsDynamically = useMemo(() => contact !== null && contact !== undefined && Object.keys(contact).map((field, index) => {
        return !isId(field) && <CustomInput key={index} value={(contact as any)[field] ?? ' '} required type={isEmailField(field) ? 'email' : 'text'}
                                            label={field}
                                            onChange={(e) => {update({...contact,[field]: e.target.value})}
                                            }/>
    }),[contact])
    const isValidEmail = new RegExp('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')

    const onSaveChangesClicked = () => {
        let errors: string[] = []
        if (!isValidEmail.test(contact.email))
        {errors.push('Email')}
        if (contact.firstName === "")
        {errors.push('Firstname')}
        if (contact.lastName === ""){errors.push('LastName')}
        if (errors.length>0){
            setErrorMessage(errors.length > 1 ? `Fields ${errors.toString()} are invalid, fix before continue`: `The Field ${errors.toString()} is invalid, fix before continue` )
            console.log(errors)
        }
        else{
            saveChanges().then((data) => {
                console.log(data)
                navigate('/')
            })
        }
    }

    return <CentralizedForm errorMessage={errorMessage} onCloseErrorMessage={() => setErrorMessage('')} hasError={errorMessage.length>0} inputs={renderFieldsDynamically} onPrimaryClick={onSaveChangesClicked} onSecondaryClick={() => navigate('/')}
                            primaryButtonText={"submit"} secondaryButtonText={"Back to Home"} />
}

export default ContactEditPage