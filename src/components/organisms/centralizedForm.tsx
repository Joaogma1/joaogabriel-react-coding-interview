import { Box } from "@mui/material";
import react, {FormEvent} from "react";
import {CustomInput} from "@components/atoms/customInput";
import {FormOptionsButton} from "@components/molecules";
import {SnackBarAlert} from "@components/molecules/snackBarAlert";

interface ICentralizedFormProps {
    inputs?: react.ReactElement<typeof CustomInput>[]
    primaryButtonText: string
    secondaryButtonText: string
    onPrimaryClick: () => void
    onSecondaryClick: () => void
    hasError?: boolean
    onCloseErrorMessage: () => void
    errorMessage?: string
}

const CentralizedForm : react.FC<ICentralizedFormProps> = ({errorMessage,onCloseErrorMessage,hasError,inputs, primaryButtonText,secondaryButtonText,onSecondaryClick,onPrimaryClick }) => {
    return (<>
        <Box onSubmit={(e) => e.preventDefault()} component="form"
                 noValidate autoComplete="off" p={8} flexDirection={"column"}
                 overflow={"auto"} display={"flex"} alignItems={"center"} justifyContent={"center"}>
                {inputs}
                <FormOptionsButton primaryButtonText={primaryButtonText} onPrimaryClick={onPrimaryClick}
                                   secondaryButtonText={secondaryButtonText} onSecondaryClick={onSecondaryClick} />
        </Box>
        <SnackBarAlert snackBarType={"error"} message={errorMessage} isOpen={hasError} onClose={onCloseErrorMessage}/>
    </>)

}
export {CentralizedForm}