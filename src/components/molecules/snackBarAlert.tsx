import react from "react";
import {CustomSnackBar} from "@components/atoms";
import {Alert} from "@mui/material";
import {AlertColor} from "@mui/material/Alert/Alert";

interface ISnackBarAlertProps {
onClose: () => void
isOpen:boolean
message: string
snackBarType:AlertColor
}
const SnackBarAlert : react.FC<ISnackBarAlertProps> = ({snackBarType, message,isOpen,onClose}) => {
    return <CustomSnackBar open={isOpen} onClose={onClose} >
        <Alert onClose={onClose} severity={snackBarType} sx={{ width: '100%' }}>{message}</Alert>
    </CustomSnackBar>
}
export {SnackBarAlert}