import react from "react";
import {Box} from "@mui/material";
import {CustomButton} from "@components/atoms";

interface IFormOptionsButtonProps {
    primaryButtonText: string
    secondaryButtonText: string
    onPrimaryClick: () => void
    onSecondaryClick: () => void
}

const FormOptionsButton : react.FC<IFormOptionsButtonProps> = ({primaryButtonText,secondaryButtonText,onSecondaryClick,onPrimaryClick}) => {
    return (
        <Box display={"flex"} width={'30%'} justifyContent={"space-between"} margin={"auto"}>
            <CustomButton variant="contained" onClick={onPrimaryClick}>{primaryButtonText}</CustomButton>
            <CustomButton variant="outlined" onClick={onSecondaryClick}>{secondaryButtonText}</CustomButton>
        </Box>
    )
}

export {FormOptionsButton}