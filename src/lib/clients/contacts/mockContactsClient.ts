import { faker } from '@faker-js/faker';

import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';
import {LocalStorageHelper} from "@lib/helpers/localStorageHelper";

function seedContacts(count: number): IPerson[] {
  const res: IPerson[] = [];

  for (let i = 0; i < count; i++) {
    res.push({
      id: faker.datatype.uuid(),
      email: faker.internet.email(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName()
    });
  }

  return res;
}

const delay = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

export class MockContactsClient implements IContactsClient {
  private readonly apiContacts: IPerson[];
  private readonly localStorage: LocalStorageHelper<IPerson[]>
  constructor(mockPersonsCount: number) {
    this.localStorage = new LocalStorageHelper<IPerson[]>();
    if (!this.localStorage.hasDataByKey("contacts")){
      this.apiContacts = seedContacts(mockPersonsCount);
      this.localStorage.add({key:"contacts", value: this.apiContacts})
    }else{
      this.apiContacts = this.localStorage.getOne("contacts")
    }
  }

  async contactList(opts: IContactListArgs): Promise<IContactListResult> {
    const { pageNumber = 1, pageSize = 10 } = opts;

    await delay(1000);
    const skip = (pageNumber - 1) * pageSize;
    const take = pageSize;

    return {
      data: this.apiContacts.slice(skip, skip + take),
      totalCount: this.apiContacts.length
    };
  }

  getContactById(id: string): Promise<IPerson> {
   return new Promise((resolve, reject) => {
     if (!this.apiContacts.some(x => x.id === id)) reject(`couldn't find Id ${id}`)
     const index = this.apiContacts.findIndex(x => x.id === id);
     resolve(this.apiContacts[index])
   })
  }
  updateContact(id: string, update: IPerson): Promise<IPerson> {
    return new Promise<IPerson>((resolve, reject) => {
      if (!this.apiContacts.some(x => x.id === id)) reject(`Could not found id ${id}`)
      const contactIndex = this.apiContacts.findIndex(x => x.id === id)
      this.apiContacts[contactIndex] = {
        id,
        ...(update.email !== null && update.email !== '') && {
          email: update.email
        },...(update.firstName !== null && update.firstName !== '') && {
          firstName: update.firstName
        },...(update.lastName !== null && update.lastName !== '') && {
          lastName: update.lastName
        },
      }
      this.localStorage.remove("contacts")
      this.localStorage.add({key:"contacts", value: this.apiContacts})
      return resolve(this.apiContacts[contactIndex])
    });
  }
}
