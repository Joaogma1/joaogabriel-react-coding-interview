import {ILocalStorageHelper, IStorageItem, StorageItem} from "@lib/helpers/localHostStorageHelperTypes";

export class LocalStorageHelper <T> implements ILocalStorageHelper<T>{
    localStorageSupported: boolean;
    constructor() {
        this.localStorageSupported = typeof window['localStorage'] != "undefined" && window['localStorage'] != null;
    }

    add(data: IStorageItem<T>): void {
        if (this.localStorageSupported) {
            localStorage.setItem(data.key, JSON.stringify(data.value));
        }
    }

    clear(): void {
        if (this.localStorageSupported) {
            localStorage.clear();
        }
    }

    getOne(key: string): T {
        if (this.localStorageSupported) {
            return (JSON.parse(localStorage.getItem(key)) as T)
        } else {
            return null;
        }
    }

    remove(key: string): void {
        if (this.localStorageSupported) {
            localStorage.removeItem(key);
        }
    }

    hasDataByKey(key: string): boolean {
        if (this.localStorageSupported) {
            return this.getOne(key) !== null
        }
    }

}