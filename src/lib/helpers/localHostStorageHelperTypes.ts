export interface IStorageItem<T> {
    key: string;
    value: T;
}

export class StorageItem<T> {
    key: string;
    value: T;

    constructor(data: IStorageItem<T>) {
        this.key = data.key;
        this.value = data.value;
    }
}

export interface ILocalStorageHelper<T> {
    add(data:IStorageItem<T>): void
    hasDataByKey(key:string): boolean
    remove(key: string): void
    clear(): void
    getOne(key: string): T
}